import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestInputComponent } from './test-input/test-input.component';

const routes: Routes = [
  {path :'', component:TestInputComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
