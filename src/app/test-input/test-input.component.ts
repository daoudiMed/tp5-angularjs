import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-test-input',
  templateUrl: './test-input.component.html',
  styleUrls: ['./test-input.component.css']
})
export class TestInputComponent implements OnInit {

  @Input()
  login:string;
  @Input()
  pass:string;

  constructor() { }

  ngOnInit() {
  }

  afficherMessage(){
    alert(this.login+" "+this.pass);
  }

}
